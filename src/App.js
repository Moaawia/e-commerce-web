import "./App.css";
import { Container } from "react-bootstrap";

import {
  Cart,
  Products,
  Categories,
  AddCategoryPage,
  LoginPage,
  SignUpPage,
  AuthRoute,
  ProtectedRoute,
  Footer,
  NavBar,
} from "./components";
import { useSelector, useDispatch } from "react-redux";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  Navigate,
} from "react-router-dom";

const App = () => {
  const user = useSelector((state) => state.auth.user);

  return (
    <div>
      {user && <NavBar isAdmin={user.isAdmin} />}
      <Routes>
        <Route
          exaxt
          path="/"
          element={
            <ProtectedRoute user={user} isAdminRoute={false}>
              <Products />
            </ProtectedRoute>
          }
        />
        <Route
          path="/my-cart"
          element={
            <ProtectedRoute user={user} isAdminRoute={false}>
              <Cart />
            </ProtectedRoute>
          }
        />
        <Route
          path="/categories"
          element={
            <ProtectedRoute user={user} isAdminRoute={true}>
              <Categories />
            </ProtectedRoute>
          }
        />
        <Route
          path="/category/add"
          exact
          element={
            <ProtectedRoute user={user} isAdminRoute={true}>
              <AddCategoryPage />
            </ProtectedRoute>
          }
        />
        <Route
          path="/category/edit/:categoryId"
          exact
          element={
            <ProtectedRoute user={user} isAdminRoute={true}>
              <AddCategoryPage />
            </ProtectedRoute>
          }
        />
        <Route
          path="/login"
          element={
            <AuthRoute user={user}>
              <LoginPage />
            </AuthRoute>
          }
        />
        <Route
          path="/signup"
          element={
            <AuthRoute user={user}>
              <SignUpPage />
            </AuthRoute>
          }
        />
        <Route path="*" element={<Navigate to="/" />} />
      </Routes>
      {user && <Footer />}
    </div>
  );
};

export default App;
