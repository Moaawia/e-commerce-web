import axios from "axios";
import { store } from "../redux/store"; // Replace with your Redux store import

export const baseURL = "http://localhost:8000/";

export const axiosInstance = axios.create({
  baseURL,
  // Set the base URL or other configurations for your Axios instance
});

// axiosInstance.interceptors.request.use((config) => {
//   const token = Cookies.get("token"); // Retrieve the token from the cookie
//   console.log("token " + token);
//   if (token) {
//     // Add the bearer token to the Authorization header
//     config.headers.Authorization = `Bearer ${token}`;
//   }

//   return config;
// });

export const configureAxios = () => {
  axiosInstance.interceptors.request.use(
    async (config) => {
      const user = store.getState().auth.user;
      config.headers.Authorization = user?.token ? `Bearer ${user.token}` : "";
      return config;
    },
    (error) => {
      return Promise.reject(error);
    }
  );
};
