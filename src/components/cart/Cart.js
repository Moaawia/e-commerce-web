import React, { useEffect } from "react";
import { Alert, Button, Card, Col, Container, Row } from "react-bootstrap";
import { fetchCartItems, emptyCart } from "../../redux/slices/cartSlice";
import { FaTrash } from "react-icons/fa";

import CartItem from "./CartItem";
import { useSelector, useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";

const Cart = () => {
  const cartState = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  useEffect(() => {
    dispatch(fetchCartItems());
  }, [dispatch]);

  const handleCheckout = () => {
    console.log("Checkout clicked");
  };

  const handleContinueShopping = () => {
    navigate("/");
    console.log("Continue Shopping clicked");
  };

  const handleEmptyCart = () => {
    dispatch(emptyCart(cartState.items));
  };

  return (
    <Container className="container-main">
      <Row className="align-items-center justify-content-between">
        <Col>
          <h1>My Cart</h1>
        </Col>
        <Col className="d-flex justify-content-end">
          {cartState.items.length > 0 && (
            <Button
              onClick={handleEmptyCart}
              variant="danger"
              className="d-flex align-items-center"
            >
              <FaTrash className="mr-2" size={20} />
              Empty Cart
            </Button>
          )}
        </Col>
      </Row>
      {cartState.items.length === 0 ? (
        <div className="d-flex align-items-center justify-content-center empty-cart-container">
          <div className="empty-cart-text text-center">
            <p>Your cart is empty.</p>
            <p>Start adding items to your cart.</p>
            <Button onClick={handleContinueShopping} variant="primary">
              Show All Products
            </Button>
          </div>
        </div>
      ) : (
        <>
          {cartState.items.map((item, index) => (
            <div key={item.id}>
              <CartItem item={item} />
              {index !== cartState.items.length - 1 && (
                <hr className="divider" />
              )}
            </div>
          ))}

          <Row className="justify-content-center">
            <Col xs={12} lg={6} className="order-details">
              <h3>Order Details</h3>
              <p>Total Price: ${cartState.totalPrice.toFixed(2)}</p>
              <p>Shipping Method: Express</p>
              <p>Delivery Address: 123 Main St, City, Country</p>
              <Button onClick={handleCheckout} variant="primary">
                Checkout
              </Button>{" "}
              <Button onClick={handleContinueShopping} variant="secondary">
                Continue Shopping
              </Button>
              {/* Add more order details */}
            </Col>
          </Row>
        </>
      )}
      <div className="bottom-alert-container">
        {cartState.success && (
          <Alert className="bottom-alert" variant="success" dismissible>
            {cartState.success}
          </Alert>
        )}
      </div>
    </Container>
  );
};

export default Cart;
