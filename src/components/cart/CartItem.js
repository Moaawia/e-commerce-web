import React from "react";
import { Card, Button, Row, Col } from "react-bootstrap";
import { BsPlus, BsDash, BsTrash } from "react-icons/bs";

import { useSelector, useDispatch } from "react-redux";
import { updateCartItem, deleteCartItem } from "../../redux/slices/cartSlice";
import { baseURL } from "../../axios";

const CartItem = ({ item, onDelete, onIncrease, onDecrease }) => {
  const { id, image, name, quantity, price } = item;

  const dispatch = useDispatch();

  const handleUpdate = (item) => {
    dispatch(updateCartItem(item));
  };

  const handleDelete = (id) => {
    dispatch(deleteCartItem(id));
  };

  return (
    <Card className="mb-3 border-0">
      <Card.Body>
        <Row className="align-items-center">
          <Col xs={6} sm={4} md={2}>
            <Card.Img src={baseURL + image} alt={name} className="item-image" />
          </Col>
          <Col xs={6} sm={4} md={3}>
            <Card.Title>{name}</Card.Title>
          </Col>
          <Col xs={6} sm={4} md={3}>
            <div className="d-flex align-items-center">
              <Button
                variant="secondary"
                disabled={quantity < 1}
                size="Xlg"
                onClick={() => {
                  if (quantity > 1)
                    handleUpdate({ ...item, quantity: quantity - 1 });
                }}
                className="plus-minus-button bg-transparent border-0"
              >
                <BsDash className="plus-icon" />
              </Button>
              <span className="quantity bordered">{quantity}</span>
              <Button
                variant="secondary"
                onClick={() =>
                  handleUpdate({ ...item, quantity: quantity + 1 })
                }
                className="plus-minus-button bg-transparent border-0"
              >
                <BsPlus className="plus-icon" />
              </Button>
            </div>
          </Col>
          <Col xs={6} sm={4} md={3}>
            <div className="price bold-text">
              {(price * quantity).toFixed(2) + "$"}
            </div>
          </Col>
          <Col xs={4} sm={2} md={1}>
            <Button
              variant="danger"
              className="delete bg-transparent border-0"
              onClick={() => handleDelete(id)}
              style={{ color: "red" }}
            >
              Delete
            </Button>
          </Col>
        </Row>
      </Card.Body>
    </Card>
  );
};

export default CartItem;
