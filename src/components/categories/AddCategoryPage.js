import React, { useState, useEffect } from "react";
import { Form, Button, Container, Alert } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import Resizer from "react-image-file-resizer";

import {
  fetchCategories,
  updateCategoryWithImage,
  addCategoryWithImage,
} from "../../redux/slices/categoriesSlice";
import { useParams } from "react-router-dom";
import { baseURL } from "../../axios";
import { useNavigate } from "react-router-dom";

const AddCategoryPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const categories = useSelector((state) => state.categories);

  const [previewURL, setPreviewURL] = useState(null);
  const [category, setCategory] = useState({
    name: "",
    description: "",
    image: null,
  });

  const { categoryId } = useParams();

  useEffect(() => {
    if (categories.items.length == 0) dispatch(fetchCategories());
  }, [dispatch]);

  useEffect(() => {
    if (categoryId) {
      const existingCategory = categories.items.find(
        (cat) => cat.id == categoryId
      );
      if (existingCategory) {
        setCategory(existingCategory);
      }
    }
  }, [categoryId, categories]);

  useEffect(() => {
    if (categories.status === "Added" || categories.status === "Updated") {
      navigate(-1);
    }
  }, [categories.status]);

  const handleChange = (e) => {
    if (e.target.name === "image") {
      const file = e.target.files[0];
      setCategory((prevCategory) => ({ ...prevCategory, image: file }));

      if (file) {
        const imageURL = URL.createObjectURL(file);
        setPreviewURL(imageURL);
      }
    } else {
      const { name, value } = e.target;
      setCategory((prevCategory) => ({ ...prevCategory, [name]: value }));
    }
  };

  const handleSubmit = (e) => {
    e.preventDefault();

    if (typeof category.image !== "string") {
      // Resize the image before sending
      Resizer.imageFileResizer(
        category.image,
        800,
        800,
        "JPEG",
        80,
        0,
        (compressedFile) => {
          if (categoryId) {
            dispatch(
              updateCategoryWithImage({
                ...category,
                id: categoryId,
                image: compressedFile,
              })
            );
          } else {
            dispatch(
              addCategoryWithImage({ ...category, image: compressedFile })
            );
          }
        },
        "file" // output type
      );
    } else {
      if (categoryId) {
        dispatch(updateCategoryWithImage({ id: categoryId, ...category }));
      } else {
        dispatch(addCategoryWithImage(category));
      }
    }

    // Reset the form after submission
    setCategory({
      name: "",
      description: "",
      image: null,
    });
    setPreviewURL(null);
    console.log(category);
  };

  return (
    <Container className="container-main">
      <h1>{categoryId ? "Edit Category" : "Add Category"}</h1>
      <Form onSubmit={handleSubmit}>
        <Form.Group className="mb-3" controlId="name">
          <Form.Label>Name</Form.Label>
          <Form.Control
            type="text"
            name="name"
            value={category.name}
            onChange={handleChange}
            required
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="description">
          <Form.Label>Description</Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            name="description"
            value={category.description}
            onChange={handleChange}
            required
          />
        </Form.Group>
        <Form.Group className="mb-3" controlId="image">
          <Form.Label>Image</Form.Label>
          <Form.Control
            type="file"
            accept="image/*"
            name="image"
            onChange={handleChange}
            required={category.image == null}
          />
        </Form.Group>
        <Form.Group>
          {previewURL && (
            <img
              className="mb-3"
              src={previewURL}
              alt="Category Preview"
              style={{ maxWidth: "200px" }}
            />
          )}
          {!previewURL && typeof category.image === "string" && (
            <img
              className="mb-3"
              src={baseURL + category.image}
              alt="Category Preview"
              style={{ maxWidth: "200px" }}
            />
          )}
        </Form.Group>
        <Button variant="primary" type="submit">
          {categoryId ? "Update Category" : "Add Category"}
        </Button>
      </Form>

      <div className="bottom-alert-container">
        {categories.error && (
          <Alert className="bottom-alert" variant="danger" dismissible>
            {categories.error}
          </Alert>
        )}
      </div>
    </Container>
  );
};

export default AddCategoryPage;
