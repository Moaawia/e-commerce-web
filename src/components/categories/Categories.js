import React, { useEffect } from "react";
import { Alert, Button, Col, Container, Row } from "react-bootstrap";
import CategoryTable from "./CategoriesTable";
import { useSelector, useDispatch } from "react-redux";
import {
  fetchCategories,
  deleteCategory,
  resetStatus,
} from "../../redux/slices/categoriesSlice";
import { useNavigate } from "react-router-dom";

const Categories = () => {
  const categoriesState = useSelector((state) => state.categories);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const handleAddCategory = () => {
    dispatch(resetStatus());
    navigate(`/category/add`);
  };

  const handleEditCategory = (categoryId) => {
    dispatch(resetStatus());
    navigate(`/category/edit/${categoryId}`);
  };

  const handleDeleteCategory = (categoryId) => {
    dispatch(deleteCategory(categoryId));
  };

  useEffect(() => {
    dispatch(fetchCategories());
  }, [dispatch]);

  return (
    <Container className="container-main">
      <Row className="mb-3">
        <Col>
          <h1>Categories</h1>
        </Col>
        <Col className="d-flex justify-content-end">
          <Button variant="primary" onClick={handleAddCategory}>
            Add New Category
          </Button>
        </Col>
      </Row>
      <CategoryTable
        categories={categoriesState.items}
        onEdit={handleEditCategory}
        onDelete={handleDeleteCategory}
      />

      <div className="bottom-alert-container">
        {(categoriesState.status === "Added" ||
          categoriesState.status === "Updated" ||
          categoriesState.status === "Deleted") && (
          <Alert className="bottom-alert" variant="success" dismissible>
            {"Category " + categoriesState.status + " Succcessfylly"}
          </Alert>
        )}
      </div>
    </Container>
  );
};

export default Categories;
