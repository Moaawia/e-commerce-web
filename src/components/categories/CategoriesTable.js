import React from "react";
import { Table, Button } from "react-bootstrap";
import { RiEdit2Line, RiDeleteBinLine } from "react-icons/ri";
import { baseURL } from "../../axios";

const CategoryTable = ({ categories, onEdit, onDelete }) => {
  return (
    <Table striped bordered hover>
      <thead>
        <tr>
          <th>ID</th>
          <th>Image</th>
          <th>Name</th>
          <th>Description</th>
          <th>Actions</th>
        </tr>
      </thead>
      <tbody>
        {categories.map((category) => (
          <tr key={category.id}>
            <td>{category.id}</td>
            <td>
              <img
                src={baseURL + category.image}
                alt="Category Preview"
                style={{ maxWidth: "100px" }}
              />
            </td>
            <td>{category.name}</td>
            <td>{category.description}</td>
            <td>
              <Button variant="link" onClick={() => onEdit(category.id)}>
                <RiEdit2Line size={20} />
              </Button>
              <Button variant="link" onClick={() => onDelete(category.id)}>
                <RiDeleteBinLine size={20} />
              </Button>
            </td>
          </tr>
        ))}
      </tbody>
    </Table>
  );
};

export default CategoryTable;
