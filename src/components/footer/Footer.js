import React from "react";
import { Container, Row, Col } from "react-bootstrap";

const Footer = () => {
  return (
    <Container fluid className="footer-container fixed-bottom">
      <Row className="justify-content-center">
        <Col className="text-center">
          <p>
            &copy; 2023 - <span>Moaawia</span>
          </p>
        </Col>
      </Row>
    </Container>
  );
};

export default Footer;
