export { default as Products } from "../components/porducts/Products";
export { default as Cart } from "../components/cart/Cart";
export { default as Categories } from "./categories/Categories";
export { default as AddCategoryPage } from "./categories/AddCategoryPage";
export { default as LoginPage } from "./auth/LoginPage";
export { default as SignUpPage } from "./auth/SignUpPage";

export { default as ProtectedRoute } from "./routers/ProtectedRoute";
export { default as AuthRoute } from "./routers/AuthRoute";

export { default as NavBar } from "./navbar/Navbar";
export { default as Footer } from "./footer/Footer";
