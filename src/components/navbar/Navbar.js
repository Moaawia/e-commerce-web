import { Navbar, Nav, Container, Button } from "react-bootstrap";
import { useSelector, useDispatch } from "react-redux";
import { IoMdCart } from "react-icons/io";
import { MdExitToApp } from "react-icons/md";
import { logout } from "../../redux/slices/authSlice";

const NavBar = ({ isAdmin }) => {
  const authState = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const handleLogout = () => {
    dispatch(logout());
  };

  return (
    <Navbar bg="dark" variant="dark" fixed="top">
      <Container>
        <Container>
          <Navbar.Brand href="/" className="d-flex align-items-center">
            <img
              src="/./logo.svg"
              width="50"
              height="50"
              className="d-inline-block align-top"
              alt="Logo"
            />
            <span className="ms-3">E-Commerce Website</span>
          </Navbar.Brand>
        </Container>
        <Navbar.Toggle />
        <Navbar.Collapse className="d-flex justify-content-end align-items-center">
          {!isAdmin && (
            <Nav className="me-3">
              <Nav.Link href="/my-cart" className="d-flex align-items-center">
                <IoMdCart size={30} />
              </Nav.Link>
            </Nav>
          )}

          <Nav className="me-3">
            <Nav.Link
              onClick={handleLogout}
              className="d-flex align-items-center"
            >
              <MdExitToApp size={30} />
            </Nav.Link>
          </Nav>
          <div className="signed-in-text">
            <Navbar.Text>
              Signed in as {isAdmin ? "Admin" : ""}:{" "}
              <span style={{ color: "white" }}>{authState.user.name}</span>
            </Navbar.Text>
          </div>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default NavBar;
