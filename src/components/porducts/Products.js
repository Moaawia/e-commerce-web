import React, { useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import Product from "./product/Product";
import { useSelector, useDispatch } from "react-redux";
import { fetchProducts } from "../../redux/slices/productsSlice";
import { addCartItem, fetchCartItems } from "../../redux/slices/cartSlice";

const Products = () => {
  const productsState = useSelector((state) => state.products);
  const cartState = useSelector((state) => state.cart);
  const authState = useSelector((state) => state.auth);
  const dispatch = useDispatch();

  const addToCart = (product) => {
    const { id, ...cartItem } = product;
    dispatch(
      addCartItem({
        ...cartItem,
        productId: id,
        quantity: 1,
        userId: authState.user.id,
      })
    );
  };

  useEffect(() => {
    dispatch(fetchProducts());
    dispatch(fetchCartItems());
  }, [dispatch]);

  const products = useSelector((state) => productsState.items);

  return (
    <Container fluid className="container-main">
      <div />
      <Row
        className="justify-content-center"
        xs={1}
        sm={2}
        md={3}
        lg={4}
        xl={4}
      >
        {products.map((product) => (
          <Col key={product.id}>
            <Product
              product={product}
              onAddToCart={() => addToCart(product)}
              isAddedToCart={cartState.items.some(
                (item) => item.productId === product.id
              )}
            />
          </Col>
        ))}
      </Row>
    </Container>
  );
};

export default Products;
