import React from "react";
import { Card, Button } from "react-bootstrap";
import { FaShoppingCart } from "react-icons/fa";
import { useNavigate } from "react-router-dom";
import { baseURL } from "../../../axios";

const Product = (props) => {
  const { name, description, price, image, id } = props.product;
  const { onAddToCart, isAddedToCart } = props;

  const navigate = useNavigate();

  return (
    <Card className="product-card">
      <div className="product-image-container">
        <Card.Img
          variant="top"
          src={baseURL + image}
          className="product-image"
        />
        <div className="product-overlay">
          <Button
            variant="primary"
            className="add-to-cart-button"
            onClick={() => {
              if (isAddedToCart) navigate("/my-cart");
              else onAddToCart();
            }}
          >
            <FaShoppingCart /> {isAddedToCart ? "Show in Cart" : "Add to Cart"}
          </Button>
        </div>
      </div>
      <Card.Body>
        <Card.Title className="card-title">{name}</Card.Title>
        <Card.Text className="product-price text-danger">
          {price + "$"}
        </Card.Text>
        <Card.Text
          className="product-description"
          dangerouslySetInnerHTML={{ __html: description }}
        />
      </Card.Body>
    </Card>
  );
};

export default Product;
