import { Navigate } from "react-router-dom";
const AuthRoute = ({ user, children }) => {
  if (user) {
    return (
      <Navigate to={user?.isAdmin === true ? "/categories" : "/"} replace />
    );
  }
  return children;
};
export default AuthRoute;
