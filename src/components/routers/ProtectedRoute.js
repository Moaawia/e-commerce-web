import { Navigate } from "react-router-dom";
const ProtectedRoute = ({ user, isAdminRoute, children }) => {
  if (!user) {
    return <Navigate to="/login" replace />;
  }
  if (user.isAdmin && !isAdminRoute)
    return <Navigate to="/categories" replace />;
  if (!user.isAdmin && isAdminRoute) return <Navigate to="/" replace />;
  return children;
};
export default ProtectedRoute;
