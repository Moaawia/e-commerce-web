import { createSlice, nanoid, createAsyncThunk } from "@reduxjs/toolkit";
import { axiosInstance } from "../../axios/index";

export const login = createAsyncThunk("/auth/login", async (credentials) => {
  try {
    const response = await axiosInstance.post(`/users/login`, credentials);
    const result = response.data;

    return result;
  } catch (error) {
    console.log(error);
    throw new Error(error.response.data.message);
  }
});

export const signUp = createAsyncThunk("/auth/signUp", async (userInfo) => {
  try {
    const response = await axiosInstance.post(`/users`, userInfo);
    const result = response.data;

    return result;
  } catch (error) {
    console.log(error);
    throw new Error(error.response.data.message);
  }
});

const initialState = {
  user: null,
  status: "loading",
  error: null,
};

const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    logout: (state, action) => {
      console.log("logging out");
      state.user = null;
    },
    clearErrors: (state, action) => {
      state.error = null;
    },
  },
  extraReducers: {
    [login.pending]: (state, action) => {
      state.error = null;
      state.status = "loading";
      state.error = null;
    },
    [login.fulfilled]: (state, action) => {
      state.status = "succeeded";
      state.user = action.payload;
    },
    [login.rejected]: (state, action) => {
      state.status = "failed";
      state.error = action.error.message;
    },
    [signUp.pending]: (state, action) => {
      state.error = null;
      state.status = "loading";
      state.error = null;
    },
    [signUp.fulfilled]: (state, action) => {
      state.status = "succeeded";
      state.user = action.payload;
    },
    [signUp.rejected]: (state, action) => {
      state.status = "failed";
      state.error = action.error.message;
    },
  },
});

export const { logout, clearErrors } = authSlice.actions;
export default authSlice.reducer;
