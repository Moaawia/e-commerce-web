import { createSlice, nanoid, createAsyncThunk } from "@reduxjs/toolkit";
import { axiosInstance } from "../../axios/index";

export const fetchCartItems = createAsyncThunk(
  "cart/fetchCartItems",
  async () => {
    const response = await axiosInstance.get("/cartItems");
    return response.data;
  }
);

export const updateCartItem = createAsyncThunk(
  "cart/updateCartItem",
  async (item) => {
    try {
      const response = await axiosInstance.put(`/cartItems/${item.id}`, item);
      const result = response.data;

      return result;
    } catch (error) {
      console.log(error);
      throw new Error(
        error?.response?.data?.message || error.message || "Error"
      );
    }
  }
);

export const addCartItem = createAsyncThunk(
  "cart/addCartItem",
  async (item) => {
    try {
      const response = await axiosInstance.post(`/cartItems`, item);
      const result = response.data;

      return result;
    } catch (error) {
      console.log(error);
      throw new Error(
        error?.response?.data?.message || error.message || "Error"
      );
    }
  }
);

export const deleteCartItem = createAsyncThunk(
  "cart/deleteCartItem",
  async (id) => {
    try {
      const response = await axiosInstance.delete(`/cartItems/${id}`);
      const result = response.data;

      return result;
    } catch (error) {
      console.log(error);
      throw new Error(
        error?.response?.data?.message || error.message || "Error"
      );
    }
  }
);

export const emptyCart = createAsyncThunk("cart/emptyCart", async (items) => {
  try {
    const response = await axiosInstance.delete(`/cartItems`);
    const result = response.data;

    return result;
  } catch (error) {
    console.log(error);
    throw new Error(error?.response?.data?.message || error.message || "Error");
  }
});

const initialState = {
  items: [],
  totalPrice: 0,
  status: "loading",
  success: null,
  error: null,
};

const cartSlice = createSlice({
  name: "cart",
  initialState,
  reducers: {},
  extraReducers: {
    [fetchCartItems.pending]: (state, action) => {
      state.error = null;
      state.status = "loading";
    },
    [fetchCartItems.fulfilled]: (state, action) => {
      state.status = "succeeded";
      state.error = null;
      state.success = null;
      console.log(action.payload);
      state.items = action.payload;
      state.totalPrice = action.payload.reduce(
        (sum, item) => sum + item.price * item.quantity,
        0
      );
    },
    [fetchCartItems.rejected]: (state, action) => {
      state.status = "failed";
      state.error = action.error.message;
    },
    [addCartItem.fulfilled]: (state, action) => {
      state.status = "succeeded";
      console.log(action.payload);
      state.items.push(action.payload);
      state.totalPrice = state.items.reduce(
        (sum, item) => sum + item.price * item.quantity,
        0
      );
    },
    [addCartItem.rejected]: (state, action) => {
      state.error = action.error.message;
    },
    [updateCartItem.pending]: (state, action) => {
      state.items = state.items.map((item) => {
        if (item.id === action.meta.arg.id) {
          return action.meta.arg;
        }
        return item;
      });
      state.totalPrice = state.items.reduce(
        (sum, item) => sum + item.price * item.quantity,
        0
      );
    },
    [deleteCartItem.pending]: (state, action) => {
      // state.items = state.items.filter((item) => item.id !== action.meta.arg);
      state.success = null;
    },
    [deleteCartItem.fulfilled]: (state, action) => {
      state.status = "success";
      state.success = "Item deleted successfylly";
      state.items = state.items.filter((item) => item.id !== action.meta.arg);
    },
    [emptyCart.fulfilled]: (state, action) => {
      state.success = null;
    },
    [emptyCart.fulfilled]: (state, action) => {
      state.items = [];
      state.status = "success";
      state.success = "Cart emptied successfylly";
    },
  },
});

export default cartSlice.reducer;
