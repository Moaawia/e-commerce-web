import { createSlice, nanoid, createAsyncThunk } from "@reduxjs/toolkit";
import { axiosInstance } from "../../axios";

const initialState = {
  items: [],
  status: "loading",
  error: null,
};

export const fetchCategories = createAsyncThunk(
  "categories/fetchCategories",
  async () => {
    try {
      const response = await axiosInstance.get("/categories");
      return response.data;
    } catch (error) {
      console.log(error);
      throw new Error(
        error?.response?.data?.message || error.message || "Error"
      );
    }
  }
);

const updateCategory = async (category) => {
  try {
    const response = await axiosInstance.put(
      `/categories/${category.id}`,
      category
    );
    return response.data;
  } catch (error) {
    console.log(error);
    throw new Error(error?.response?.data?.message || error.message || "Error");
  }
};

const addCategory = async (category) => {
  try {
    const response = await axiosInstance.post("/categories", category);
    return response.data;
  } catch (error) {
    console.log(error);
    throw new Error(error?.response?.data?.message || error.message || "Error");
  }
};

export const updateCategoryWithImage = createAsyncThunk(
  "categories/updateCategory",
  async (category) => {
    try {
      if (typeof category.image !== "string") {
        const formData = new FormData();
        formData.append("image", category.image);
        const imageResponse = await axiosInstance.post("/images", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
        return await updateCategory({
          ...category,
          image: imageResponse.data.imageName,
        });
      } else return await updateCategory(category);
    } catch (error) {
      console.log(error);
      throw new Error(
        error?.response?.data?.message || error.message || "Error"
      );
    }
  }
);

export const addCategoryWithImage = createAsyncThunk(
  "categories/addCategory",
  async (category) => {
    try {
      if (typeof category.image !== "string") {
        const formData = new FormData();
        formData.append("image", category.image);
        const imageResponse = await axiosInstance.post("/images", formData, {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        });
        return await addCategory({
          ...category,
          image: imageResponse.data.imageName,
        });
      } else return await addCategory(category);
    } catch (error) {
      console.log(error);
      throw new Error(
        error?.response?.data?.message || error.message || "Error"
      );
    }
  }
);

export const deleteCategory = createAsyncThunk(
  "categories/deleteCategory",
  async (categoryId) => {
    const response = await axiosInstance.delete(`/categories/${categoryId}`);
    return response.data;
  }
);

const categoriesSlice = createSlice({
  name: "categories",
  initialState,
  reducers: {
    resetStatus: (state, action) => {
      state.status = null;
    },
  },
  extraReducers: {
    [fetchCategories.pending]: (state, action) => {
      state.error = null;
      // state.status = "loading";
    },
    [fetchCategories.fulfilled]: (state, action) => {
      // state.status = "fetched";
      state.items = action.payload;
    },
    [fetchCategories.rejected]: (state, action) => {
      state.status = "failed";
      state.error = action.error.message;
    },
    [addCategoryWithImage.pending]: (state, action) => {
      state.error = null;
      state.status = "loading";
    },
    [addCategoryWithImage.fulfilled]: (state, action) => {
      state.status = "Added";
    },
    [addCategoryWithImage.rejected]: (state, action) => {
      state.status = "failed";
      state.error = action.error.message;
    },
    [updateCategoryWithImage.pending]: (state, action) => {
      state.error = null;
      state.status = "loading";
    },
    [updateCategoryWithImage.fulfilled]: (state, action) => {
      state.status = "Updated";
    },
    [updateCategoryWithImage.rejected]: (state, action) => {
      state.status = "failed";
      console.log(action.error.message);
      state.error = action.error.message;
    },
    [deleteCategory.pending]: (state, action) => {
      console.log(action.meta.arg);
      state.items = state.items.filter((item) => item.id != action.meta.arg);
    },
    [deleteCategory.fulfilled]: (state, action) => {
      state.status = "Deleted";
    },
  },
});
export const { resetStatus } = categoriesSlice.actions;
export default categoriesSlice.reducer;
