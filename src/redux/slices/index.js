import { combineReducers } from "@reduxjs/toolkit";
import cartReducer from "./cartSlice";
import productsReducer from "./productsSlice";
import authReducer from "./authSlice";
import categoriesReducer from "./categoriesSlice";

const rootReducer = combineReducers({
  cart: cartReducer,
  products: productsReducer,
  auth: authReducer,
  categories: categoriesReducer,
});

export default rootReducer;
